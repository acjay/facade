"""
Used to create dumb facade objects to cache results of time-consuming
operations on underlying objects.
"""

import functools


def standard_key_function(obj):
    """
    Returns the object as its own unique identifier
    :param obj: the object to be matched
    """
    return obj


def django_model_key_function(obj):
    """
    Returns the class name plus primary key as the unique identifier

    standard_key_function doesn't work for Django models because the
    same model queried separately will be different Python objects. If
    that behavior is desired, then use that function.
    :param obj: the object to be matched
    """
    return obj.__class__, obj.pk


def memoize(wrapped):
    """
    Decorator to memoize functions that take one or more arguments for
    use with facades.

    :param wrapped: function to be memoized
    """

    @functools.wraps(wrapped)
    def mock(self, *args, **kwargs):
        """
        Mock function that runs wrapped and remembers the result,
        which stands in for wrapped

        :param self: explicit self
        :param args:
        :param kwargs:
        """

        if not isinstance(self, Facade):
            raise Exception('memoize is meant to wrap only Facade objects')

        key = (wrapped, args, tuple(kwargs.items()))
        if key not in self._cache:
            self._cache[key] = wrapped(self, *args, **kwargs)
        return self._cache[key]
    return mock


class FacadeFactory(object):
    """
    Creates a new FacadeFactory to cache facade objects, so that
    facade objects do not need to be passed around.  The client of the
    facade object can instantiate a "new" facade object for an
    underyling object, and the factory will return the original facade
    object if one exists. This way, facade method calls are cached just
    once over the lifetime of the factory.

    In Django, factories are created for the purpose of serving an
    individual request, under the assumption that any view
    participating in the request will be expecting the same values from
    repeated facade method calls. The middleware provided with this app
    can be used to create a new factory for each request.
    """

    # Stores object class/facade class mapping
    registry = {}

    @classmethod
    def register(cls, objClass, key_function=django_model_key_function):
        """
        Registers a facade class to be instantiated for a given
        object's class
        :param objClass: the object class to match
        :param key_function: function used to identify and memoize
            object instances.
        """

        def apply_register(facadeClass):
            """
            Actual decorator function, closed upon the object class
            passed into the decorator generator
            :param facadeClass: the facade class to instantiate
            """
            cls.registry[objClass] = (facadeClass, key_function)
        return apply_register

    def __init__(self):
        # Start with an empty cache
        self.cache = {}

    def __call__(self, obj):
        """
        Calling a FacadeFactory instance returns a new facade instance
        appropriate for the class of the object that's passed to the
        factory, based on registration.  It also memoizes the
        constructed facades so that only one facade exists per object.
        """

        try:
            (facadeClass, key_function) = FacadeFactory.registry[obj.__class__]
        except KeyError:
            return obj

        key = key_function(obj)
        if key not in self.cache:
            newFacade = facadeClass(obj)
            newFacade._f = self
            self.cache[key] = newFacade

        return self.cache[key]


class Facade(object):
    """
    Base class for object facades

    Implements caching of data requested for high-level attributes that
    might normally be calculated at great time cost. Generally, all
    information to be required should be implemented as individual
    property-decorated methods. The facade is meant to be a thin, dumb,
    denormalized container for information on the underlying object.
    Property and method calls are also passed directly through to the
    underlying object, bypassing the caching behavior when desired.

    Facades intantiated from a factory come with a method called _f
    that returns the factory that instantiated the facade, useful for
    wrapping objects that come from underlying object methods.
    """

    def __init__(self, obj):
        super(Facade, self).__init__()
        self._obj = obj
        self._cache = {}

    def __getattribute__(self, item):
        """
        Caches all attribute access
        """

        if item == '__class__':
            # Done to spoof isinstance, so that Django equality checks
            # work
            return self._obj.__class__
        elif item in ['_cache', '_obj', '_f'] + object.__dict__.keys():
            return object.__getattribute__(self, item)

        if item not in self._cache:
            self._cache[item] = object.__getattribute__(self, item)
        return self._cache[item]

    def __getattr__(self, item):
        """
        Pass calls through to the model when the attribute is
        undefined on the Facade

        This should only be used for attributes that are always
        retrieved when the object is queried, either because they are
        model attributes or because select_related was used.  It is up
        to the developer to know when this is the case.
        """

        if hasattr(self._obj, item):
            return getattr(self._obj, item)

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self._obj == other

    def __ne__(self, other):
        return not self.__eq__(other)