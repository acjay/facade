"""
The Facade app has no models.  This module exists for two reasons:

1) To allow the app to be tested in the normal Django way
2) To allow users to define which modules to import, causing the
   FacadeFactory registrations
"""

from django.conf import settings

if hasattr(settings, 'FACADE_MODULES'):
    for module in settings.FACADE_MODULES:
        __import__(module)