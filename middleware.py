"""
Attaches a Facade factory to the request
"""

from .facade import FacadeFactory


class FacadeMiddleware(object):
    """
    Attaches a FacadeFactory to the request that handles the caching of
    objects used in that request
    """

    def process_request(self, request):
        """
        Add a new facade factory to the current request

        :param request: current request
        """
        request.facade = FacadeFactory()