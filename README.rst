Facade by Breakrs
=================

Facade is an interface for creating proxy objects to cache results of
expensive "getter" operations on underlying objects.

Purpose
-------

This module was designed with Django models and views in mind. In
particular, it is meant to work with apps that have large views
connected with templates that delegate more view work to subview
template tags. In other words, multi-page architecture apps with rich
interfaces. Facades are used to cache expensive queries that might be
used in the view and multiple components on a page. This modifies the
model-view-template framework into a model-facade-views-templates
framework.

On the surface, this seems more complicated, but in fact, using facades
allows you to pull nearly all complex model querying logic out of the
views and centralizes it in the facades, eliminating the need for
CBV-style mixins and providing natural DRYness. Additionally, using
facades tends to produce the nice, flat data structures prefered by
Django templates.

Facades are also designed to work with minimal effort. No need to
reimplement the entire interface of your underlying objects--facades
pass through calls to the underlying objects they wrap, allowing the
facade to stand in for the underlying object itself. Note that pass-
through calls will not be cached, so this behavior is best only relied
upon for calling simple methods that don't trigger related-
object queries.

FacadeFactory and middleware
----------------------------

This app also includes middleware to add a FacadeFactory object to
every Django request before it reaches your view. This object stashes
existing facades globally per request. This greatly reduces the need to
pass the results of expensive queries around your view-subview
hierarchy. Just pass the facade object, or feed a regular object to the
factory to get the single matching facade object. If it doesn't exist,
it will be created. If it does, the single cached copy is given to you.

Given the single facade instance for that underlying object, trigger as
many expensive queries as you want. The actual query will only hit the
database once per request. All other times, the cached result will be
returned.

Registration
------------

All Facade classes must be registered with the FacadeFacotry at app
launch time.  There are three ways to accomplish this.

1.  Keep Facade-derived classes in your ``views`` module
2.  Keep them elsewhere (such as a ``facades`` module), in which case
    you have two sub-options:
    a.  Import that module in ``views``
    b.  Add the module as a string to the FACADE_MODULES setting (e.g
        ``FACADE_MODULES = ('myapp.facades',)``)

Installation
------------

1.  Add ``'facade'`` to your ``INSTALLED_APPS``
2.  Add ``'facade.middleware.FacadeMiddleware'`` to your
    ``MIDDLEWARE_CLASSES``

Todos
-----

 -  Example code

Metainformation
---------------

Author
......

Alan Johnson

+   alan@breakrs.com
+   http://twitter.com/alanbreakrs
+   http://bitbucket.org/acjohnson55

Copyright and license
.....................

Copyright 2012 Breakrs, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this work except in compliance with the License.
You may obtain a copy of the License in the LICENSE file, or at:

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
