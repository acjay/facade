"""
Tests for Facade app
"""

# pylint: disable=R0901,R0904,R0915,C0321

import random
from django.conf.urls import patterns, url
from django.http import HttpResponse

from django.test import TestCase

from .facade import (
    Facade, FacadeFactory, standard_key_function, django_model_key_function,
    memoize)


class TestClass(object):
    """
    Simple test class
    """

    initialValue = 0

    def __init__(self):
        """Initialize test object"""
        self.value = TestClass.initialValue

    def test_method(self):
        """Method that returns different values each time it is called"""
        self.value += 1
        return self.value


class UnregesteredTestClass(object):
    """
    Intentionally unregistered test class
    """
    def test_method(self):
        """Stub mthod for testing"""
        pass


@FacadeFactory.register(TestClass, standard_key_function)
class TestClassFacade(Facade):
    """
    Simple test facade
    """

    @property
    def test_result(self):
        """
        Caches TestClass.test_method
        """
        return self._obj.test_method()


class FakeDjangoClass(object):
    """
    Fake django class
    """

    short_query_initial_value = 7
    long_query_initial_value = 19

    def __init__(self, pk):
        self.pk = pk
        self.short_query_value = FakeDjangoClass.short_query_initial_value
        self.long_query_value = FakeDjangoClass.long_query_initial_value

    def short_query(self):
        """Test function for pass-through behavior"""
        self.short_query_value += 1
        return self.short_query_value

    def long_query(self):
        """Test function for basic caching"""
        self.long_query_value += 1
        return self.long_query_value

    # noinspection PyUnusedLocal
    def multi_value_function(self, _arg1, _arg2, _arg3):
        """
        Test function memoizing functions based on input params.

        Facade should store output values for this function based on
        the distinct combination of input values it is called with.

        :param _arg1: arg1
        :param _arg2: arg2
        :param _arg3: arg3
        """
        return random.random()

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.pk == other.pk

    def __ne__(self, other):
        return not self.__eq__(other)


@FacadeFactory.register(FakeDjangoClass, django_model_key_function)
class FakeDjangoClassFacade(Facade):
    """Test facade"""

    @property
    def long_result(self):
        """Test basic property memoization"""
        return self._obj.long_query()

    @memoize
    def multi_value_result(self, argA, argB, argC):
        """Test :py:func:memoize

        :param argA: arg1
        :param argB: arg2
        :param argC: arg3
        """
        return self._obj.multi_value_function(argA, argB, argC)

def fake_view(request):
    """Nonsense view to test middleware"""

    facade1 = request.facade(FakeDjangoClass(pk=1))
    facade2 = request.facade(FakeDjangoClass(pk=1))

    # Long result should be cached, even though two separate instances
    # were used to generate the facades, because the facades are cached
    # in the FacadeFactory, put on the request with the middleware.
    html = "<html><body>{0}</body></html>".format(
        facade1.long_result == facade2.long_result)
    return HttpResponse(html)


urlpatterns = patterns(
    '',
    url(
        r'^$',
        fake_view
    )
)


class FacadeTestCase(TestCase):
    """Unit tests for facade infrastructure"""

    urls = 'facade.tests'

    def test_standard_facade(self):
        """
        Make sure caching of facade objects and their properties works
        """

        factory = FacadeFactory()

        obj1 = TestClass()
        obj2 = TestClass()

        obj1Facade1 = factory(obj1)
        obj1Facade2 = factory(obj1)
        obj2Facade = factory(obj2)

        # Make sure only one facade was created, and that it was
        # returned
        self.assertIs(obj1Facade1, obj1Facade2)

        # Make sure factory returns different facades for different
        # objects
        self.assertIsNot(obj1Facade1, obj2Facade)

        # Make sure method results are being cached
        self.assertEqual(obj1Facade1.test_result, TestClass.initialValue + 1)
        self.assertEqual(obj1Facade1.test_result, TestClass.initialValue + 1)

        obj3 = UnregesteredTestClass()
        self.assertIsInstance(factory(obj3), UnregesteredTestClass)
        self.assertRaises(Exception, memoize(UnregesteredTestClass.test_method))

    def test_django_facade(self):
        """
        Make sure caching behaves properly for Django-style objects
        """

        factory = FacadeFactory()

        # Different objects, but should have the same facade since they
        # have the same pk
        djangoObj1 = FakeDjangoClass(pk=1)
        djangoObj2Copy = FakeDjangoClass(pk=1)

        djangoObj1Facade = factory(djangoObj1)
        djangoObj1CopyFacade = factory(djangoObj2Copy)

        self.assertIs(djangoObj1Facade, djangoObj1CopyFacade)

        # Make sure method results are being cached
        self.assertEqual(
            djangoObj1Facade.long_result,
            FakeDjangoClass.long_query_initial_value + 1)
        self.assertEqual(
            djangoObj1Facade.long_result,
            FakeDjangoClass.long_query_initial_value + 1)

        # Test pass-through, which should *not* be memoized
        self.assertEqual(
            djangoObj1Facade.short_query(),
            FakeDjangoClass.short_query_initial_value + 1)
        self.assertEqual(
            djangoObj1Facade.short_query(),
            FakeDjangoClass.short_query_initial_value + 2)

        testValue = djangoObj1Facade.multi_value_result(2, 3, argC=4)

        # Should work because random value should be memoized, even
        # with kwargs
        self.assertEqual(
            testValue, djangoObj1Facade.multi_value_result(2, 3, argC=4))

        # Shouldn't work because random value is memoized to function
        # params
        self.assertNotEqual(
            testValue, djangoObj1Facade.multi_value_result(5, 6, argC=7))

        # Test comparison operators
        self.assertEqual(djangoObj1Facade, djangoObj1)
        self.assertEqual(djangoObj1, djangoObj1Facade)
        self.assertEqual(djangoObj1Facade, djangoObj1Facade)

        # Test middleware
        resp = self.client.get('')
        self.assertContains(resp, 'True')